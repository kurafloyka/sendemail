
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class Mail {


    public static void main(String[] args) {

        System.out.println(System.getProperty("user.dir"));

        // Recipient's email ID needs to be mentioned.
        String to = "farukakyol@windowslive.com";

        // Sender's email ID needs to be mentioned
        String from = "farukakyol3480@gmail.com";

        // Assuming you are sending email from through gmails smtp
        String host = "smtp.gmail.com";

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");

        // Get the Session object.// and pass
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication("farukakyol3480@gmail.com", "Elif8131643");

            }

        });
        //session.setDebug(true);
        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("Subject kismi icin bu bir test otomasyon islemidir.");

            Multipart multipart = new MimeMultipart();

            MimeBodyPart attachmentPart = new MimeBodyPart();
            MimeBodyPart attachmentPart2 = new MimeBodyPart();

            MimeBodyPart textPart = new MimeBodyPart();

            try {

                File f = new File(System.getProperty("user.dir") +"/pom.xml");

                attachmentPart.attachFile(f);
                textPart.setText("Body kismi icin bu bir test otomasyon islemidir.");
                multipart.addBodyPart(textPart);
                multipart.addBodyPart(attachmentPart);

                File f1 = new File(System.getProperty("user.dir") +"/src/main/java/Mail.java");
                attachmentPart2.attachFile(f1);
                multipart.addBodyPart(attachmentPart2);


            } catch (IOException e) {

                e.printStackTrace();

            }

            message.setContent(multipart);

            System.out.println("sending...");
            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }

    }

}

