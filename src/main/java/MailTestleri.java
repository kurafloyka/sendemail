import org.junit.Test;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MailTestleri {

    // Recipient's email ID needs to be mentioned.
    String to = "esmmfinans@allianz.com.tr";
    // Sender's email ID needs to be mentioned
    String from = "farukakyol3480@gmail.com";
    // Assuming you are sending email from through gmails smtp
    String host = "smtp.gmail.com";
    // Get system properties
    Properties properties;
    // Get the Session object.// and pass
    Session session;
    String userName = "farukakyol3480@gmail.com";
    String password = "Elif8131643";
    MimeMessage message;
    Multipart multipart;
    MimeBodyPart attachmentPart, attachmentPart2, textPart;
    File f, f2;

    List<String> pathList;


    public void setPropertiesAndSession() {

        properties = System.getProperties();

        // Setup mail server
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");

        // Get the Session object.// and pass
        session = Session.getInstance(properties, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(userName, password);

            }

        });


    }

    public void createFileandAddMail(List<String> path) {


        multipart = new MimeMultipart();

        attachmentPart = new MimeBodyPart();
        attachmentPart2 = new MimeBodyPart();

        textPart = new MimeBodyPart();

        try {


            if (path.size() > 1) {

                f = new File(System.getProperty("user.dir") + "/" + path.get(0));

                attachmentPart.attachFile(f);
                textPart.setText("Body kismi icin bu bir test otomasyon islemidir.");
                multipart.addBodyPart(textPart);
                multipart.addBodyPart(attachmentPart);

                f2 = new File(System.getProperty("user.dir") + "/" + path.get(1));
                attachmentPart2.attachFile(f2);
                multipart.addBodyPart(attachmentPart2);


            } else {
                //System.out.println(System.getProperty("user.dir") + "/" + path.get(0));
                f = new File(System.getProperty("user.dir") + "/" + path.get(0));
                attachmentPart.attachFile(f);
                multipart.addBodyPart(attachmentPart);
            }


            //textPart.setText("Body kismi icin bu bir test otomasyon islemidir.");
            //multipart.addBodyPart(textPart);

        } catch (Exception e) {

            e.printStackTrace();

        }


    }

    public void editEmailAttachAndFile(List<String> path) {


        //session.setDebug(true);
        try {
            // Create a default MimeMessage object.
            message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("Subject kismi icin bu bir test otomasyon islemidir.");

            createFileandAddMail(path);

            message.setContent(multipart);

            System.out.println("sending...");
            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }

    }


    @Test
    public void xmlPdfToFinans() {

        pathList = new ArrayList<String>();
        pathList.add("src/main/resources/XML_PDF.pdf");

        setPropertiesAndSession();
        editEmailAttachAndFile(pathList);


    }

    @Test
    public void nonXmlAndPdfToFinans() {


        pathList = new ArrayList<String>();
        pathList.add("src/main/resources/NonXML_PDF.pdf");
        pathList.add("src/main/resources/NonXML_PDF.xml");



        setPropertiesAndSession();
        editEmailAttachAndFile(pathList);
    }

    @Test
    public void detailsXmlAndPdfToFinans() {

        pathList = new ArrayList<String>();
        pathList.add("src/main/resources/DetailsXML_PDF.pdf");
        pathList.add("src/main/resources/DetailsXML_PDF.xml");



        setPropertiesAndSession();
        editEmailAttachAndFile(pathList);
    }


}
